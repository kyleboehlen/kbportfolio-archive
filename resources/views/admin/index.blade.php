@extends('admin.base')

@section('panel')
    <div class="panel full-height placeholder d-flex flex-column justify-content-evenly align-items-center">
        <h2 class="home-header">No place like 127.0.0.1 :)</h2>
        <img class="d-none d-lg-block" src="https://imgs.xkcd.com/comics/computer_problems.png" />
    </div>
@endsection